class OpenRootWebComponent extends HTMLElement {

    declare shadowRoot: ShadowRoot;

    public constructor() {
        super();
        this.attachShadow({ mode: 'open' });
    }

    /**
     * Returns an array of attributes that should be observed.
     * 
     * @return {string[]}
     */
    public static get observedAttributes(): string[] {
        return ['id'];
    }

    /**
     * Creates a template for the element.
     * 
     * @return {HTMLTemplateElement}
     */
    public get template(): HTMLTemplateElement {
        // create elements from the inside
        const slot = document.createElement('slot');

        const h1 = document.createElement('h1');
        h1.appendChild(slot);

        // finally create the template element
        const template = document.createElement('template');
        template.content.appendChild(h1);

        return template;
    }

    /**
     * Renders the element from template to the shadow root
     * 
     */
    public render(): void {
        this.shadowRoot.innerHTML = '';
        const node = this.template.content.cloneNode(true);
        this.shadowRoot.appendChild(node);
    }

    /**
     * Called when the element is added to the DOM.
     * 
     */
    public connectedCallback(): void {
        this.render();
    }

    /**
     * Called when an attribute is added, removed, or updated.
     * 
     * @param attributeName
     * @param oldValue 
     * @param newValue 
     */
    public attributeChangedCallback(attributeName: string, oldValue: string, newValue: string): void {
        this.render();
    }

    /**
     * Called when the element is removed from the DOM.
     * 
     */
    public disconnectedCallback(): void {
        // dispose DOM elements
        // remove event listeners
        // remove references
        // stop timers
    }

    /**
     * Called when the element is moved to a new document.
     * 
     */
    public adoptedCallback(): void {
        //
    }
}

customElements.define('web-component', OpenRootWebComponent);
